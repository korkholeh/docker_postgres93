FROM phusion/baseimage:0.9.10

RUN /etc/my_init.d/00_regen_ssh_host_keys.sh
CMD ["/sbin/my_init"]

RUN apt-get -qq update
RUN apt-get install -y wget psmisc mc daemontools curl
RUN locale-gen en_US
RUN locale-gen en_US.UTF-8
RUN locale-gen uk_UA
RUN locale-gen uk_UA.UTF-8
RUN locale-gen ru_RU
RUN locale-gen ru_RU.UTF-8
RUN update-locale LC_ALL=uk_UA.UTF-8 LANG=uk_UA.UTF-8

RUN apt-get -qq install postgresql-9.3 postgresql-contrib-9.3
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN rm -rf /var/lib/postgresql/9.3/main/*

ADD syslog-ng.postgresql.conf /etc/syslog-ng/conf.d/postgresql.conf
RUN echo 'host all all 0.0.0.0/0 md5' >> /etc/postgresql/9.3/main/pg_hba.conf
RUN echo "log_destination = 'stderr,syslog'" >> /etc/postgresql/9.3/main/postgresql.conf
RUN echo "syslog_facility = 'LOCAL0'" >> /etc/postgresql/9.3/main/postgresql.conf
RUN echo "syslog_ident = 'postgres'" >> /etc/postgresql/9.3/main/postgresql.conf
RUN echo "password_encryption = on" >> /etc/postgresql/9.3/main/postgresql.conf

VOLUME /var/lib/postgresql/9.3/main/
VOLUME /var/log/
VOLUME /root/.ssh

RUN echo -n uk_UA.UTF-8 > /etc/container_environment/LANG

EXPOSE 5432

RUN mkdir /etc/service/postgresql
ADD postgresql.sh /etc/service/postgresql/run
RUN chmod +x /etc/service/postgresql/run
